===========================================
 Deploying Build App Into OpenShift Server
===========================================

Windows Platform:

Pre-requisites

1) Git installed (https://git-scm.com/download/win)
2) NPM Package Installed (https://nodejs.org/en/download/)
3) Composer PHP Installed (https://getcomposer.org/download/)
4) Ruby / RubyGems installed (Ruby) (http://rubyinstaller.org/downloads/)
5) OpenShift RHC Console (Command line: gem install rhc) - Install Ruby first
	a) Issue might be encountered such as SSL certificate. To address this, execute the ff. command to change the source:
		gem sources --remove https://rubygems.org/
		gem sources --add http://rubygems.org/
	
	More details on how to setup OpenShift at this link: https://developers.openshift.com/getting-started/windows.html#client-tools

====================================
Setting-up Openshift Console for the First Time
====================================

1) In command prompt, execute 'rhc setup'
2) Follow the steps in the process, especially when uploading the RSA Key to your OpenShift server.
	Server hostname: openshift.redhat.com
3) The console will prompt you to login your OpenShift account for the first time. Enter your e-mail address and password
4) The console will prompt you generate a token now. Type 'yes' and then press Enter.
5) The console will prompt you to upload your public key saved in your computer. Type 'yes' and then press Enter.
6) Enter a desired key name for the uploaded public key. Then press Enter.

====================================
Generating RSA Key
====================================
1) Using your installed Git Bash, execute 'ssh-keygen -t rsa'
2) Save the file in in your User directory (C:\Users\myusername\.ssh\id_rsa for instance).
3) Leave the passphrase blank. Continue until the end of setup.
4) Copy your newly generated keys (all files inside it) from User directory and paste it in your Git installation directory's .ssh folder (in %PROGRAMFILES%\Git\.ssh or %PROGRAMFILES(X86)%\Git\.ssh)
	a) If there is no .ssh folder inside the git installation, use a command prompt and go to the git installation directory and execute 'mkdir .ssh' then copy the rsa files there.

====================================
Uploading the deployment version of API to OpenShift server
====================================

1) Get the source file from the repository
2) In command prompt, go to the source root directory (cd).
3) Execute 'npm install' to install dependencies from the package.json file.
4) Execute next 'composer install' to install dependencies from the composer.json file
5) Still in the root folder, execute next 'gulp build --env prod' to create a deployable folder/files.
6) Execute 'cd serve' to go to the build folder.
7) Execute 'git init' to initialize a git repository.
8) Execute 'git add .' to add modified files.
9) Execute 'git commit -m "initial commit"' to commit changes to the local repository.
10) Register a remote repository by executing 'git remote add movie-demand-api ssh://580e51a17628e116b700003b@modapi-alinan.rhcloud.com/~/git/modapi.git/'
	To double-check if added successfully, execute 'git remote -v'
11) To synchronize the remote version to local, execute first 'git pull movie-demand-api master'
12) There might some changes happened, especially in 'index.php', so perform again the 'gulp build' to restore files to original.
13) Execute again 'git add .' to add modified files.
14) Execute again 'git commit -m "second commit"' to commit the changes.
15) To sync the final product to openshift server, execute 'git push movie-demand-api master'
