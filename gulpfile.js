var gulp = require('gulp');
var inject = require('gulp-inject');
var del = require('del');
var clean = require('gulp-clean');
var replace = require('gulp-replace-task');
var fs = require('fs');
var args = require('yargs').argv;


gulp.task('copy-modules', function() {
  var lib = [
  	//'./node_modules/php-slim/slim/Slim/**/*',
  	'./vendor/slim/slim/Slim/**/*'
  ];

  return gulp.src(lib)
  	.pipe(gulp.dest('./serve/slim/Slim'));

});

gulp.task('copy-components', function() {
	var includes = [
		'./src/include/*.inc',
    './src/include/*.php',
		'./src/lib/**/*',
		'./src/.htaccess',
		'./src/index.php'
	];

	return gulp.src(includes, {base: './src'})
		.pipe(gulp.dest('./serve'));
});

gulp.task('replace-constant', function() {
	var env = args.env || 'dev';
	var file = './src/config/database.env.json';

	var settings = JSON.parse(fs.readFileSync(file,'utf8'));
  	var enVariables = {};

  	switch(env) {
		case 'prod' : enVariables = settings.prod; break;
		default: enVariables = settings.dev;
	}

	return gulp.src('./src/include/raw/database_consts.inc')
		.pipe(replace({
          patterns: [
            {
              match: 'hostName',
              replacement: enVariables.dbHostName
            },
            {
              match: 'username',
              replacement: enVariables.dbUserName
            },
            {
              match: 'password',
              replacement: enVariables.dbPassword
            },
            {
              match: 'dbName',
              replacement: enVariables.dbName
            }
          ]
	    }))
		.pipe(gulp.dest('./src/include'));
})

gulp.task('clean', function () {
  	return gulp.src('serve')
  		.pipe(clean());
});

gulp.task('build', ['replace-constant', 'copy']);
gulp.task('copy', ['copy-modules', 'copy-components']);