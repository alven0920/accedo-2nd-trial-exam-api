<?php
	
	require_once 'database_consts.inc';

	function connectToDb() {

		$connString = 'mysql:host='. DB_HOSTNAME .';dbname='. DB_NAME .';charset=utf8';

		$conn = new PDO($connString, DB_USERNAME, DB_PASSWORD);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

		return $conn;
	}

?>