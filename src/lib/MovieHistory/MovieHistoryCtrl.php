<?php

	class MovieHistoryCtrl {

		private $conn;

		public function __construct() {
			$this->conn = connectToDb();
		}

		public function getById($id) {
			$sql = "SELECT * FROM watch_history where user_id = :user_id ORDER BY last_watched DESC";
			
			$stmt = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
			$stmt->execute(array(':user_id' => $id));

			$watchArray = array();

			while($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
				array_push($watchArray, $result);
			}

			return $watchArray;
		}

		public function getAll() {
			$sql = "SELECT * FROM watch_history";
			
			$stmt = $this->conn->prepare($sql);
			$stmt->execute();

			$watchArray = array();

			while($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
				array_push($watchArray, $result);
			}

			return $watchArray;
		}

		public function add($movieHistory) {

			$userId = $movieHistory->getUserId();
			$movieId = $movieHistory->getMovieId();
			$lastWatched = $movieHistory->getLastWatched();

			$sql = "INSERT INTO watch_history(user_id, movie_id, last_watched)"
				. "VALUES (:user_id, :movie_id, :last_watched)";

			$stmt = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));

			return $stmt->execute(array(':user_id' => $userId, ':movie_id' => $movieId, ':last_watched' => $lastWatched));

		}
	}	

?>