<?php

	class MovieHistoryModel {

		private $userId;
		private $movieId;
		private $lastWatched;

		public function __construct() {

		}

		public function setUserId($userId) {
			$this->userId = $userId;
		}

		public function setMovieId($movieId) {
			$this->movieId = $movieId;
		}

		public function setLastWatched($lastWatched) {
			$this->lastWatched = $lastWatched;
		}

		// Getters
		public function getUserId() {
			return $this->userId;
		}

		public function getMovieId() {
			return $this->movieId;
		}

		public function getLastWatched() {
			return $this->lastWatched;
		}
	}	

?>