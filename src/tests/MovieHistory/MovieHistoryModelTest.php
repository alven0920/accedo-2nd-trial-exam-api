<?php
	
	require_once '../../lib/MovieHistory/MovieHistoryModel.php';

	class MovieHistoryModelTest extends PHPUnit_Framework_TestCase {

		public function setUp() { }

		public function tearDown() { }

		public function testMovieHistoryBean() {
			// ensure that bean is working properly
			$model = new MovieHistoryModel();

			$model->setmovieId('1');
			$model->setUserId('12345');
			$model->setLastWatched(date('d-M-Y H:i:s'));

			$this->assertTrue($model->getUserId() == '12345');
			$this->assertTrue($model->getMovieId() == '1');
			$this->assertTrue($model->getLastWatched() == date('d-M-Y H:i:s'));
			
		}

	}

?>