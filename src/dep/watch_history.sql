-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Host: 127.8.22.130:3306
-- Generation Time: Oct 25, 2016 at 05:28 AM
-- Server version: 5.5.50
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `modapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `watch_history`
--

CREATE TABLE IF NOT EXISTS `watch_history` (
  `user_id` varchar(40) NOT NULL,
  `movie_id` varchar(20) NOT NULL,
  `last_watched` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `watch_history`
--

INSERT INTO `watch_history` (`user_id`, `movie_id`, `last_watched`) VALUES
('12341', '12-years-a-slave', '2016-10-23 03:04:21'),
('12341', '12-years-a-slave', '2016-10-23 03:04:33'),
('12341', '22-jump-street', '2016-10-23 03:04:54'),
('12341', 'alexander', '2016-10-23 05:35:54'),
('12341', '21-jump-street', '2016-10-23 08:55:18'),
('12341', '10-things-i-hate-abo', '2016-10-24 03:02:18'),
('nb1ck42scg', '21-jump-street', '2016-10-23 00:00:00'),
('nb1ck42scg', '21-jump-street', '2016-10-23 00:00:00'),
('nb1ck42scge6133evg877l7gk5', '21-jump-street', '2016-10-23 00:00:00'),
('1ddmdcpar0e4nnr86cnbfs2gc6', '12-years-a-slave', '2016-10-24 03:36:43'),
('klf5sop8tl235pmhpqr0dng172', '300', '2016-10-24 03:43:50'),
('ad5e81u5p1pupg2n7h2h311nh5', '22-jump-street', '2016-10-24 03:45:49'),
('q2uh0jj0sfi3hn0hgnc5dpuc61', '2-guns', '2016-10-24 03:47:55'),
('gp62kqr075stpg7i9kji264cp5', '47-ronin', '2016-10-24 03:49:16'),
('gp62kqr075stpg7i9kji264cp5', 'a-nightmare-on-elm-s', '2016-10-24 03:49:43'),
('gp62kqr075stpg7i9kji264cp5', '10-things-i-hate-abo', '2016-10-24 03:50:04'),
('gp62kqr075stpg7i9kji264cp5', 'about-time', '2016-10-24 03:52:59'),
('4qdarjobc483rjpprhqrmlbni7', '2-guns', '2016-10-24 03:53:54'),
('4qdarjobc483rjpprhqrmlbni7', '22-jump-street', '2016-10-24 03:56:28'),
('4qdarjobc483rjpprhqrmlbni7', 'alien', '2016-10-24 03:57:10'),
('4qdarjobc483rjpprhqrmlbni7', '300-rise-of-an-empir', '2016-10-24 03:57:37'),
('4qdarjobc483rjpprhqrmlbni7', '300', '2016-10-24 03:58:13'),
('4qdarjobc483rjpprhqrmlbni7', '12-years-a-slave', '2016-10-24 02:00:22'),
('e7a91k6ui67oog9spv94kvvel4', '12-years-a-slave', '2016-10-24 02:26:15'),
('e7a91k6ui67oog9spv94kvvel4', '28-days-later', '2016-10-24 02:26:24'),
('e7a91k6ui67oog9spv94kvvel4', 'all-the-president-s-', '2016-10-24 03:40:16'),
('ngr7gmr2sm3m7mq2e0881i7b31', 'american-beauty', '2016-10-24 04:58:09'),
('ngr7gmr2sm3m7mq2e0881i7b31', 'a-girl-walks-home-al', '2016-10-24 05:01:37'),
('ngr7gmr2sm3m7mq2e0881i7b31', 'a-girl-walks-home-al', '2016-10-24 05:02:31'),
('ngr7gmr2sm3m7mq2e0881i7b31', '47-ronin', '2016-10-24 05:03:18'),
('ngr7gmr2sm3m7mq2e0881i7b31', '12-years-a-slave', '2016-10-24 05:09:08'),
('ngr7gmr2sm3m7mq2e0881i7b31', 'addicted', '2016-10-24 05:15:06'),
('ngr7gmr2sm3m7mq2e0881i7b31', '2-guns', '2016-10-24 05:16:13'),
('ngr7gmr2sm3m7mq2e0881i7b31', '22-jump-street', '2016-10-24 05:22:27'),
('ngr7gmr2sm3m7mq2e0881i7b31', '28-days-later', '2016-10-24 05:25:03'),
('ngr7gmr2sm3m7mq2e0881i7b31', '300-rise-of-an-empir', '2016-10-24 05:27:42'),
('ngr7gmr2sm3m7mq2e0881i7b31', '12-years-a-slave', '2016-10-24 05:33:03'),
('ngr7gmr2sm3m7mq2e0881i7b31', '2001-a-space-odyssey', '2016-10-24 05:34:00'),
('ngr7gmr2sm3m7mq2e0881i7b31', '22-jump-street', '2016-10-24 05:36:23'),
('ngr7gmr2sm3m7mq2e0881i7b31', '2-guns', '2016-10-24 05:36:39'),
('merp39sqlujg7msjbd196vuhi2', '12-years-a-slave', '2016-10-24 05:38:55'),
('merp39sqlujg7msjbd196vuhi2', '2-guns', '2016-10-24 05:42:25'),
('merp39sqlujg7msjbd196vuhi2', '10-things-i-hate-abo', '2016-10-24 05:49:05'),
('l4fpsoqkju5fnmlg14v1h204t3', '12-years-a-slave', '2016-10-24 05:49:58'),
('6u45rmb7bcn4v1mu7jh1r06vo3', '12-years-a-slave', '2016-10-24 05:51:16'),
('6u45rmb7bcn4v1mu7jh1r06vo3', '12-years-a-slave', '2016-10-24 05:53:23'),
('6u45rmb7bcn4v1mu7jh1r06vo3', '2-guns', '2016-10-24 05:53:28'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 06:04:36'),
('019j5eu1t0qduunlte5iiqkot7', '3-days-to-kill', '2016-10-24 06:04:58'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 06:10:31'),
('019j5eu1t0qduunlte5iiqkot7', '3-days-to-kill', '2016-10-24 06:10:45'),
('019j5eu1t0qduunlte5iiqkot7', '28-days-later', '2016-10-24 06:47:53'),
('019j5eu1t0qduunlte5iiqkot7', 'a-million-ways-to-di', '2016-10-24 06:50:15'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 06:52:17'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 06:54:16'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 06:57:53'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 06:59:07'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:04:25'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:04:25'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:04:50'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:04:50'),
('019j5eu1t0qduunlte5iiqkot7', '22-jump-street', '2016-10-24 07:05:30'),
('019j5eu1t0qduunlte5iiqkot7', '22-jump-street', '2016-10-24 07:06:38'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:07:25'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:07:25'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:09:45'),
('019j5eu1t0qduunlte5iiqkot7', '12-years-a-slave', '2016-10-24 07:09:45'),
('019j5eu1t0qduunlte5iiqkot7', '2001-a-space-odyssey', '2016-10-24 07:16:56'),
('019j5eu1t0qduunlte5iiqkot7', '2001-a-space-odyssey', '2016-10-24 07:16:56'),
('019j5eu1t0qduunlte5iiqkot7', '2-guns', '2016-10-24 07:19:46'),
('019j5eu1t0qduunlte5iiqkot7', '2-guns', '2016-10-24 07:19:46'),
('kaev3htsu78ta84rp3g8ll8qm6', '12-years-a-slave', '2016-10-24 07:21:13'),
('kaev3htsu78ta84rp3g8ll8qm6', '12-years-a-slave', '2016-10-24 07:21:13'),
('kaev3htsu78ta84rp3g8ll8qm6', '12-years-a-slave', '2016-10-24 07:21:41'),
('kaev3htsu78ta84rp3g8ll8qm6', '12-years-a-slave', '2016-10-24 07:21:41'),
('igbt3olujjivqdhn4fo8vudv95', '2-guns', '2016-10-24 07:23:41'),
('igbt3olujjivqdhn4fo8vudv95', '300', '2016-10-24 07:23:54'),
('igbt3olujjivqdhn4fo8vudv95', '300', '2016-10-24 07:23:54'),
('tc2jsd405497av4imqav0e71p6', '22-jump-street', '2016-10-24 07:25:19'),
('tc2jsd405497av4imqav0e71p6', '22-jump-street', '2016-10-24 07:25:19'),
('tc2jsd405497av4imqav0e71p6', '12-years-a-slave', '2016-10-24 07:25:36'),
('roijlpuh8gq76ahco9snt7gr37', '28-days-later', '2016-10-24 07:26:29'),
('roijlpuh8gq76ahco9snt7gr37', 'a-walk-among-the-tom', '2016-10-24 07:26:46'),
('roijlpuh8gq76ahco9snt7gr37', 'a-walk-among-the-tom', '2016-10-24 07:26:46'),
('roijlpuh8gq76ahco9snt7gr37', '47-ronin', '2016-10-24 07:27:39'),
('roijlpuh8gq76ahco9snt7gr37', '47-ronin', '2016-10-24 07:27:39'),
('ggbbol82mbm1ecle71cr00mpe1', '12-years-a-slave', '2016-10-24 07:29:48'),
('ggbbol82mbm1ecle71cr00mpe1', '21-jump-street', '2016-10-24 07:30:06'),
('ggbbol82mbm1ecle71cr00mpe1', '2001-a-space-odyssey', '2016-10-24 07:30:21'),
('ggbbol82mbm1ecle71cr00mpe1', '300', '2016-10-24 07:30:41'),
('ggbbol82mbm1ecle71cr00mpe1', '47-ronin', '2016-10-24 07:31:56'),
('ggbbol82mbm1ecle71cr00mpe1', '2001-a-space-odyssey', '2016-10-24 07:38:58'),
('ggbbol82mbm1ecle71cr00mpe1', 'addicted', '2016-10-24 07:47:29'),
('ggbbol82mbm1ecle71cr00mpe1', '12-years-a-slave', '2016-10-24 07:48:07'),
('ggbbol82mbm1ecle71cr00mpe1', 'about-time', '2016-10-24 08:09:06'),
('ggbbol82mbm1ecle71cr00mpe1', '10-things-i-hate-abo', '2016-10-24 08:16:58'),
('ggbbol82mbm1ecle71cr00mpe1', 'alexander-and-the-te', '2016-10-24 08:18:40'),
('ggbbol82mbm1ecle71cr00mpe1', '12-years-a-slave', '2016-10-24 08:33:53'),
('bftm37401qhl05e4s17f206vb6', '10-things-i-hate-abo', '2016-10-24 08:56:55'),
('bftm37401qhl05e4s17f206vb6', 'about-time', '2016-10-24 08:58:09'),
('bftm37401qhl05e4s17f206vb6', 'alien-3', '2016-10-24 09:02:58'),
('bftm37401qhl05e4s17f206vb6', '2001-a-space-odyssey', '2016-10-24 09:30:07'),
('bftm37401qhl05e4s17f206vb6', '21-jump-street', '2016-10-24 09:38:44'),
('bftm37401qhl05e4s17f206vb6', '300', '2016-10-24 09:39:53'),
('bftm37401qhl05e4s17f206vb6', '28-days-later', '2016-10-24 09:42:07'),
('bftm37401qhl05e4s17f206vb6', '47-ronin', '2016-10-24 09:45:28'),
('bftm37401qhl05e4s17f206vb6', '2001-a-space-odyssey', '2016-10-24 09:46:11'),
('bftm37401qhl05e4s17f206vb6', 'a-clockwork-orange', '2016-10-24 09:48:31'),
('bftm37401qhl05e4s17f206vb6', '47-ronin', '2016-10-24 09:49:30'),
('bftm37401qhl05e4s17f206vb6', '28-days-later', '2016-10-24 09:49:48'),
('bftm37401qhl05e4s17f206vb6', 'a-nightmare-on-elm-s', '2016-10-24 09:52:37'),
('bftm37401qhl05e4s17f206vb6', '21-jump-street', '2016-10-24 09:53:15'),
('qn2i01nutkqkpjl6deknr218d7', '10-things-i-hate-abo', '2016-10-24 11:28:03'),
('qn2i01nutkqkpjl6deknr218d7', '2-guns', '2016-10-24 11:29:25'),
('qn2i01nutkqkpjl6deknr218d7', '12-years-a-slave', '2016-10-24 11:56:59'),
('qn2i01nutkqkpjl6deknr218d7', '22-jump-street', '2016-10-25 12:45:14'),
('qn2i01nutkqkpjl6deknr218d7', 'a-walk-among-the-tom', '2016-10-25 12:47:09'),
('qn2i01nutkqkpjl6deknr218d7', '2-guns', '2016-10-25 12:50:00'),
('qn2i01nutkqkpjl6deknr218d7', '300-rise-of-an-empir', '2016-10-25 12:50:19'),
('qn2i01nutkqkpjl6deknr218d7', '2001-a-space-odyssey', '2016-10-25 01:59:52'),
('qn2i01nutkqkpjl6deknr218d7', '12-years-a-slave', '2016-10-25 02:00:11'),
('mrsk107vhajnuf6nb5e11l0s36', '21-jump-street', '2016-10-25 12:17:38'),
('f32vt9bhrtn81ka1p6m543oaq6', 'alexander-and-the-te', '2016-10-25 12:25:46'),
('6tlc6f43h2m60arsq7dk94p984', '12-years-a-slave', '2016-10-25 12:26:32'),
('338virpjb9v99t4plglh0p9ur3', '12-years-a-slave', '2016-10-25 12:28:23'),
('v3oq55hhaf5ke90g14nd94r375', '2001-a-space-odyssey', '2016-10-25 12:28:30'),
('n7uic81vlaf9sjnufeeqmmf964', '2-guns', '2016-10-25 12:29:40'),
('k8fpcaj0fq356ung7iooflhml0', '3-days-to-kill', '2016-10-25 12:30:14'),
('Array', '12-years-a-slave', '2016-10-25 01:09:42'),
('Array', '12-years-a-slave', '2016-10-25 01:10:34'),
('Array', '2-guns', '2016-10-25 01:13:25'),
('Array', '12-years-a-slave', '2016-10-25 01:15:22'),
('Array', '2-guns', '2016-10-25 01:17:05'),
('Array', '2-guns', '2016-10-25 01:19:20'),
('1cbdca50ebabdc5f8b2ec1f4ca', '2-guns', '2016-10-25 01:21:47'),
('Array', '2-guns', '2016-10-25 01:23:59'),
('759bc8fad9ac1cee554dd8f69c', '2-guns', '2016-10-25 01:25:31');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
