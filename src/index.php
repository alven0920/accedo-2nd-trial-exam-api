<?php

	require(__DIR__ . DIRECTORY_SEPARATOR . "slim" . DIRECTORY_SEPARATOR . "Slim" . DIRECTORY_SEPARATOR . "Slim.php");
	

	\Slim\Slim::registerAutoloader();

	$app = new \Slim\Slim();

	$app->get('/GetMovieHistory/:id', function($id) {
		session_start();

		header('Access-Control-Allow-Origin: *');
	    header("Access-Control-Allow-Methods: *");
	    header('Content-Type: application/json');

	    require('./include/connect.php');
		require './lib/MovieHistory/MovieHistoryCtrl.php';

		$ctrl = new MovieHistoryCtrl();

		$result = $ctrl->getById($id);

		$data = array();

		$data['sessionId'] = $id;
		$data['moviesHistory'] = $result;

		echo json_encode($data);

	});

	$app->post('/AddMovieHistory', function() use ($app) {

		session_start();

		header('Access-Control-Allow-Origin: *');
	    header("Access-Control-Allow-Methods: *");
	    header('Content-Type: application/json');

		require('./include/connect.php');
		require('./lib/MovieHistory/MovieHistoryCtrl.php');
		require('./lib/MovieHistory/MovieHistoryModel.php');

		$model = new MovieHistoryModel();

		$movieId = $app->request->post('movieId');
		$userId = $app->request->post('userId');
		$lastWatched = $app->request->post('lastWatched');

		$model->setMovieId($movieId);
		$model->setUserId($userId);
		$model->setLastWatched($lastWatched);

		$ctrl = new MovieHistoryCtrl();

		$result = $ctrl->add($model);
		
		$data = array();

		$data['sessionId'] = $userId;

		if($result) {
			$data['message'] = 'added successfully';	
		} else {
			$data['message'] = 'transaction failed';
		}
		

		echo json_encode($data);
	});

	$app->get('/HomePage', function() {
		echo '<h1>Hello. This is the VOD REST Service</h1>';
	});

	$app->run();

?>